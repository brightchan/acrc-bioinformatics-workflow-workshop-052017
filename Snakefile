#shell.executable("/bin/bash")# FIXME needed?
# shell.prefix("source whatever;") eou pipefail auto?

REFFA = "ref/Ecoli_K12_MG1655_NC_000913.fa"
FQPAIRS = dict()
FQPAIRS['ERR178187'] = [
    ['fastq/ERR178187_L1_R1.fastq.gz', 'fastq/ERR178187_L1_R2.fastq.gz'],
    ['fastq/ERR178187_L2_R1.fastq.gz', 'fastq/ERR178187_L2_R1.fastq.gz']]
REF_REGIONS = ["NC_000913:0-1000000",
               "NC_000913:1000000-2000000",
               "NC_000913:2000000-3000000",
               "NC_000913:3000000-4000000",
               "NC_000913:4000000-4639675"]


onstart:
    sys.stderr.write("Starting...\n")
onsuccess:
    sys.stderr.write("Workflow execution successful.\n")
onerror:
    sys.stderr.write("Workflow execution failed. Sad!\n")


def list_fastqcs(wc):
    fqs = []
    for sample, pairs in FQPAIRS.items():
        for pair in pairs:
            for fq in pair:
                fq = os.path.basename(fq.replace(".fastq.gz", "_fastqc.html"))
                fqs.append(os.path.join("fastqc", fq))
    return fqs


rule all:
    input:
        fastqcs = list_fastqcs,
        bams = expand("bam/{sample}.bam", sample=FQPAIRS),
        vcfs = expand("vcf/{sample}_lofreq.vcf.gz", sample=FQPAIRS)
        #  FIXME report

        
rule fastqc:
    input:
        expand("fastq/{{sample}}_L{lane}_R{readno}.fastq.gz", lane=["1", "2"], readno=["1", "2"])
        #"{prefix}.fastq.gz"        
    output:
        expand("fastqc/{{sample}}_L{lane}_R{readno}_fastqc.html", lane=["1", "2"], readno=["1", "2"])
        #"{prefix}_fastqc.html"
    log:
        "fastqc/{sample}.fastqc.log"
        #"{prefix}_fastqc.html.log"
    message:
        "Running FastQC (log: {log})"
    threads:
        1
    shell:
        "fastqc -o fastqc -t {threads} {input} >& {log}"
        # note: output file not (cannot be) named explicitely

        
rule mapreads:
    input:
        reffa = REFFA,
        refidx = REFFA + ".pac",
        fastq = ["fastq/{sample}_L{lane}_R1.fastq.gz", "fastq/{sample}_L{lane}_R2.fastq.gz"]
    output:
        bam = temp("bam/{sample}_L{lane}.bam")
    log:
        "bam/{sample}_L{lane}.bam.log"
    message:
        "Mapping reads (log: {log})"
    threads:
        8
    shell:
        "{{ bwa mem -t {threads} {input.reffa} {input.fastq} | samtools sort -T aa -@ {threads} -o {output.bam} -; }} >& {log}"


        
def list_lane_bams_per_samples(wildcards):
    bams = []
    for fqpair in FQPAIRS[wildcards.sample]:
        bambase = os.path.basename(fqpair[0]).replace("_R1.fastq.gz", ".bam")
        bams.append(os.path.join("bam", bambase))
    return bams
                        
    
rule bammerge:
    input:
        bams = list_lane_bams_per_samples
    output:
        bam = "{dirname}/{sample}.bam"
    log:
        "{dirname}/{sample}.bam.log"
    message:
        "Merging BAM files"
    threads:
        2
    shell:
        "samtools merge -@ {threads} {output.bam} {input.bams} >& {log}"


rule faindex:
    input:
        "{prefix}.{suffix}"
    output:
        "{prefix}.{suffix,(fasta|fa)}.fai"
    log:
        "{prefix}.{suffix,(fasta|fa)}.fai.log"
    message:
        "Indexing {input}"
    shell:
        "samtools faidx {input} >& {log}"


# on cluster: consider adding to actual bam creating rule
rule bamindex:
    input:
        "{prefix}.bam"
    output:
        "{prefix}.bam.bai"
    log:
        "{prefix}.bam.bai.log"
    message:
        "Indexing {input}"
    threads:
        1
    shell:
        "samtools index -@ {threads} {input} >& {log}"

        
rule lofreq:
    input:
        bam = "bam/{sample}.bam",
        bamindex = "bam/{sample}.bam.bai",
        reffa = REFFA,
        reffaidx = REFFA + ".fai"
    output:
        temp("vcf/{sample}_lofreq.{region}.vcf.gz")
    log:
        "vcf/{sample}_lofreq.{region}.vcf.gz.log"
    message:
        "Calling variants with LoFreq on {input}"
    threads:
        1
    params:
        region = lambda wc: wc.region
    shell:
        """lofreq call -B -b 1 -r {params.region} -f {input.reffa} {input.bam} -o {output}"""


localrules: merge_vcf
rule merge_vcf:
    input:
        expand("vcf/{{sample}}_lofreq.{region}.vcf.gz", region=REF_REGIONS)
    output:
        "vcf/{sample}_lofreq.vcf.gz"
    log:
        "vcf/{sample}_lofreq.vcf.gz.log"
    message:
        "Merging VCF files"
    shell:
        "bcftools merge -O z -o {output} {input}"
        
    
