args="--timestamp
--printshellcmds
--configfile conf.yaml
--latency-wait 60
--max-jobs-per-second 1
--cluster-config cluster.yaml
--jobs 10
--drmaa \" -pe OpenMP {threads} -l mem_free={cluster.mem} -l h_rt={cluster.time} -cwd -e clusterlogs -o clusterlogs -w n -v PATH=/mnt/software/stow/lofreq_star-2.1.2/bin/:\$PATH\"
"

# --use-conda

# NOTE: we have several problems with --use-conda in our setup
# see e.g. https://bitbucket.org/snakemake/snakemake/issues/536/conda-environments-not-able-to-be
# so we cheat here, don't --use-conda and add lofreq to path because it's not in the
# acrc-genomics-pipeline-workshop env
echo DRMAA_LIBRARY_PATH=\$SGE_ROOT/lib/lx-amd64/libdrmaa.so snakemake $args | tr '\n' ' '
echo
