# Slides and Source Code for the ACRC Genomics Workflow Workshop (2017-05-05)

## Slides

See genomics-pipeline-workshop-acrc-05-2017.pptx (or genomics-pipeline-workshop-acrc-05-2017.pdf)


## Data preparation

This section explains how FastQ files and reference fasta were
created. Note that the FastaQ splitting is artificial, i.e. the data
was not actually sequenced in different lanes.

### FastQs

    mkdir fastq
    cd fastq

    for sample in ERR178187 ERR178228; do
        wget -nd ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR178/$sample/${sample}_1.fastq.gz;
    	wget -nd ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR178/$sample/${sample}_2.fastq.gz;
    	gzip -dc ${sample}_1.fastq.gz | split -l 2010000 - ${sample}_LX_R1.;
    	gzip -dc ${sample}_2.fastq.gz | split -l 2010000 - ${sample}_LX_R2.;
        i=0; for f in ${sample}_LX_R1*; do let i=i+1; mv $f $(echo $f |  sed -e "s,X,$i," -e "s,.[a-z][a-z]$,.fastq,"); done;
        i=0; for f in ${sample}_LX_R2*; do let i=i+1; mv $f $(echo $f |  sed -e "s,X,$i," -e "s,.[a-z][a-z]$,.fastq,"); done;
    	ls *fastq | xargs -P 2 -n 1 gzip;
    done

### Reference Fasta

    cd ref
    gzip -dc Ecoli_K12_MG1655_NC_000913.fa.gz > Ecoli_K12_MG1655_NC_000913.fa
    # original source: https://www.ncbi.nlm.nih.gov/nuccore/NC_000913, reformatted as needed and saved as
    # Ecoli_K12_MG1655_NC_000913.fa
    samtools faidx Ecoli_K12_MG1655_NC_000913.fa
    bwa index Ecoli_K12_MG1655_NC_000913.fa

## Author

Andreas Wilm <wilma@gis.a-star.edu.sg>

## License


This is an Open Access article distributed under the terms of the
Creative Commons Attribution License
(http://creativecommons.org/licenses/by/4.0/), which permits
unrestricted reuse, distribution, and reproduction in any medium,
provided the original work is properly cited.
