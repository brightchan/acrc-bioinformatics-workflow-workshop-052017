SAMPLES = ["ERR178187", "ERR178228"]

rule all:
    input:
         #["out/ERR178187.bam", "out/ERR178228.bam"]
         expand("out/{sample}.bam", sample=SAMPLES)
    
rule align:
    input:
        reffa = "ref/Ecoli_K12_MG1655_NC_000913.fa",
        fastqs = ["fastq/{sample}_10k_1.fastq.gz", "fastq/{sample}_10k_2.fastq.gz"]
    output:
        bam = "out/{sample}.bam"
    log:
        "out/{sample}.bam.log"        
    message:
        "Aligning reads to create {output}"
    params:
        rg = "@RG\tID:{sample}\tSM:{sample}"
    threads:
        4
    shell:
        "{{ bwa mem -T {threads} -R '{params.rg}' {input.reffa} {input.fastqs} | "
        " samtools view -o {output.bam}; }} >& {log}"
        
