rule align:
    input:
        reffa = "ref/Ecoli_K12_MG1655_NC_000913.fa",
        fastqs = ["fastq/ERR178187_10k_1.fastq.gz", "fastq/ERR178187_10k_2.fastq.gz"]
    output:
        bam = "out/ERR178187.bam"
    log:
        "out/ERR178187.bam.log"        
    message:
        "Aligning reads to create {output}"
    params:
        rg = "@RG\tID:ERR178187\tSM:ERR178187"
    threads:
        4
    shell:
        "{{ bwa mem -T {threads} -R '{params.rg}' {input.reffa} {input.fastqs} | "
        " samtools view -o {output.bam}; }} >& {log}"
        
