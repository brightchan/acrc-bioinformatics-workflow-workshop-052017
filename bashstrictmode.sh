#!/bin/bash

# This script contains three errors, all easily
# detected by using the unoffical bash strict
# mode (see below). Without that the exit status
# is 0 and the final output file exists

#set -e
#set -u
#set -o pipefail

FQ1=fastq/ERR178187_L1_R1.fastq.gz
FQ2=fastq/ERR178187_Ll_R2.fastq.gz
REF=ref/Ecoli_K12_MG1655_NC_000913.fa
OUTBAM=/tmp/out.bam
test -f $OUTBAM && rm -f $OUTBAM;

ech "Starting"

bwa mem $REF $FQ1 $FQ2 | samtools view -o $OUTBA -;

status=$?
test -f $OUTBAM && exists=yes || exists=no
echo "Result:"
echo " - Exit status  $status (0 == success)"
echo " - Output exists: $exists"


    
